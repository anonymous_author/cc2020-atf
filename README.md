# Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters

## Introduction
This artifact describes the experiments conducted in the paper *Efficient Auto-Tuning of Parallel Programs with Interdependent Tuning Parameters* submitted to the [ACM SIGPLAN 2020 International Conference on Compiler Construction](https://conf.researchr.org/home/CC-2020). The artifact is provided as a Docker image and alternatively also as plain source code files. Detailed instructions on how to run the experiments can be found in [Experiment workflow using Docker](https://gitlab.com/anonymous_author/cc2020-atf#experiment-workflow-using-docker) and [Experiment workflow without Docker](https://gitlab.com/anonymous_author/cc2020-atf#experiment-workflow-without-docker). In case of **any problems**, please feel free to **open an issue** to get in touch with the authors.

**<div align="center">Note that in its default version, the artifact performs only a subset of experiments described in the paper; thereby, we reduce artifact's execution time to ~2.5h (note: performing all experiments requires ~19 hours). The user can conveniently adapt artifact's default settings, e.g., enable/disable experiments, using configuration file `config.json` which is located in artifact's root directory. Moreover, the user can use file `config.json` to enable evaluating ATF's overall auto-tuning efficiency (not presented in the paper) for two applications listed in Table 1 of our paper: i) BLAS routines [30], and ii) Probabilistic Record Linkage [33].</div>**

We compare ATF to [CLTune](https://github.com/CNugteren/CLTune) version 2.7.0 and [OpenTuner](https://github.com/jansel/opentuner) version 0.8.0. We take our GEMM running example from the open-source library [CLBlast](https://github.com/CNugteren/CLBlast) version 1.5.0. 

## Software dependencies
The artifact is delivered as a Docker image, thus freeing the user from installing any software dependencies except for Docker itself. The Docker image contains the OpenCL runtime environment for Intel CPUs. In case the user intends to evaluate our experiments on devices different from Intel CPU, the corresponding driver and OpenCL runtime have to be installed inside the Docker container. The Docker image is based on Ubuntu 16.04 -- additional software can be installed inside the Docker container using the APT package manager. Note that when targeting NVIDIA GPUs, it is necessary to install the exact same NVIDIA driver version inside the Docker container that is also used on the host system for the GPU.

In case the user intends to execute our experiments without using Docker, the following software dependencies have to be installed:
- an OpenCL driver and runtime environment
- Python 2.7 (not Python 3.x)
- Python packages:
  - OpenTuner 0.8.0
  - pybind11
  - PyOpenCL
- Clang compiler supporting C++14 or higher
- CMake 2.8.11 or higher
- R with packages ggplot2 and optparse installed (only required when results should be automatically plotted)
- Java runtime

## Experiment workflow using Docker
The user is invited to perform the following steps:
1. Obtain Docker image:
    
    `$ wget https://gitlab.com/anonymous_author/cc2020-atf/raw/master/atf_artifact_docker.tar.gz`

    `$ tar -xzf atf_artifact_docker.tar.gz`
2. Load our Docker image:
    
    `$ docker load < atf_artifact_docker.tar`
3. Run our Docker image:
    
    `$ docker run --name atf_artifact -it atf_artifact /bin/bash`
    
    In case the user intends to execute our experiments on a GPU, additional flags have to be passed to our Docker image. For this, first, a list of the required device files has to be obtained; for example, this is done for a GPU from NVIDIA as follows:
    
    `$ ls /dev/nvidia*`
    
    All listed device files have to be appended to the `docker run` command using the `--device` flag as follows:
    
    ```
    $ docker run --device=/dev/nvidia0:/dev/nvidia0
        --device=/dev/nvidia-uvm:/dev/nvidia-uvm
        --device=/dev/nvidiactl:/dev/nvidiactl
        --name atf_artifact -it atf_artifact /bin/bash
    ```
    __Important:__ Note that in order to use an NVIDIA GPU, the exact same version of the NVIDIA driver used on the host system has to be installed by the user inside our Docker container; for this, the `apt` command can be used.
4. Inside our Docker container, change into the artifact's root directory:
    
    `$ cd /artifact`
5. Select the desired OpenCL platform and device id by editing file `config.json` in line 2 and 3; available platforms and devices, as well as their corresponding ids, can be listed by executing command `clinfo`.
6. Run our experiments:
    
    `$ ./scripts/run_all_experiments.sh`
7. Run the references:
    
    `$ ./scripts/run_all_references.sh`
8. Plot the graphs:
    
    `$ ./scripts/plot_graphs.sh`

    The generated graphs can be copied from the docker container to user's home folder by executing command `$ docker cp atf_artifact:/artifact/graphs/ ~/`

In case the user accidentally terminates the Docker container, it can be restored by executing command `docker start -ia atf_artifact`.

__Note:__ When changing the platform or device id in file `config.json`, make sure to clean previous results by executing `$ ./scripts/clean_results.sh`.

## Experiment workflow without Docker
The user is invited to perform the following steps:
1. Download the artifact's archive:
    
    `$ wget https://gitlab.com/anonymous_author/cc2020-atf/raw/master/atf_artifact.tar.gz`
2. Unpack the archive:
    
    `$ tar -xzf atf_artifact.tar.gz`

3. Change into artifact's root directory:

    `$ cd artifact`

    Set the environment variable `ARTIFACT_DIR` to artifact's root directory; for this, execute

    ```
    $ export ARTIFACT_DIR=`pwd`
    ```

    inside the artifact's directory.
       
4. Compile artifact's source code:

    `$ ./scripts/install.sh`

    All arguments provided to script `install.sh` will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found by CMake (e.g., when dependencies are not installed in their default locations).

5. Select the desired OpenCL platform and device id by editing file `config.json` in line 2 and 3; available platforms and devices, as well as their corresponding ids, can be listed by executing command `clinfo`.
6. Run our experiments:
    
    `$ ./scripts/run_all_experiments.sh`
7. Run the references:
    
    `$ ./scripts/run_all_references.sh`
8. Plot the graphs:
    
    `$ ./scripts/plot_graphs.sh`

__Note:__ When changing the platform or device id in file `config.json`, make sure to clean previous results by executing `$ ./scripts/clean_results.sh`.

## Optional: Evaluating ATF's overall auto-tuning efficiency for BLAS

Note that this artifact additionally allows evaluating ATF's overall auto-tuning efficiency for BLAS [30] and Probabilistic Record Linkage [33] (not presented in the paper), as discussed above.

For this, additionally the following command has to be executed between steps 6 and 7 (independently of using Docker or not) to auto-tune our competitor CLBlast via CLTune:
    
`$ ./scripts/tune_clblast.sh`
    
__Note:__ When changing the platform or device id in file `config.json`, make sure to rerun the CLBlast tuning by calling the script above again.
